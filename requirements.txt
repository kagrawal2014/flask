Flask==0.10.1
Flask-Login==0.2.10
Flask-OpenID==1.2.1
Flask-SQLAlchemy==1.0
Flask-WTF==0.9.5
Jinja2==2.7.2
MarkupSafe==0.19
SQLAlchemy==0.9.4
Tempita==0.5.2
WTForms==1.0.5
Werkzeug==0.9.4
decorator==3.4.0
# gviz-api.py==1.8.2
itsdangerous==0.24
migrate==0.2.2
pbr==0.8.0
psycopg2==2.5.2
python-openid==2.2.5
six==1.6.1
sqlalchemy-migrate==0.9
