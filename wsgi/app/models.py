from app import db
import datetime
import json

class mlperf(db.Model):
    __tablename__ = "mlperf"
    id=db.Column(db.Integer, primary_key=True)
    timestamp=db.Column(db.DateTime)
    release=db.Column(db.Float)
    install_time=db.Column(db.Integer)
    mldb_size=db.Column(db.VARCHAR)
    insert_time=db.Column(db.Integer)
    inserted_db=db.Column(db.VARCHAR)
    report_time=db.Column(db.Integer)
    report_name=db.Column(db.VARCHAR)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        #return '<DATE %r>; <RELEASE %d>; <install_time %d>; <db_size %r>; <insert_time %d>; <inserted_db %r>; <report_time %r>; <report_name %r>' %(self.timestamp, self.release, self.install_time, self.db_size, self.insert_time, self.inserted_db, self.report_time, self.report_name)
        #return '<DATE %r>; <RELEASE %f>; <install_time %d>; <db_size %r>;' %(self.timestamp, self.release, self.install_time, self.db_size)
        data={}
        for key in self.__table__.columns.keys():
            val = getattr(self, key)
            if type(val) is datetime.datetime:
                val = val.strftime('%Y-%m-%dT%H:%M:%SZ')
            data[key] = val
        return json.dumps(data)

class mcperf(db.Model):
    __tablename__ = "mcperf"
    id=db.Column(db.Integer, primary_key=True)
    timestamp=db.Column(db.DateTime)
    release=db.Column(db.Float)
    elapsed_time=db.Column(db.Integer)
    mc_tool=db.Column(db.VARCHAR)
    avg_cpu_util=db.Column(db.Float)
    max_cpu_util=db.Column(db.Float)
    avg_mem_util=db.Column(db.Float)
    max_mem_util=db.Column(db.Float)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        #return '<DATE %r>; <RELEASE %d>; <install_time %d>; <db_size %r>; <insert_time %d>; <inserted_db %r>; <report_time %r>; <report_name %r>' %(self.timestamp, self.release, self.install_time, self.db_size, self.insert_time, self.inserted_db, self.report_time, self.report_name)
        #return '<DATE %r>; <RELEASE %f>; <install_time %d>; <db_size %r>;' %(self.timestamp, self.release, self.install_time, self.db_size)
        data={}
        for key in self.__table__.columns.keys():
            val = getattr(self, key)
            if type(val) is datetime.datetime:
                val = val.strftime('%Y-%m-%dT%H:%M:%SZ')
            data[key] = val
        return json.dumps(data)

class mdperf(db.Model):
    __tablename__ = "mdperf"
    id=db.Column(db.Integer, primary_key=True)
    timestamp=db.Column(db.DateTime)
    release=db.Column(db.Float)
    elapsed_time=db.Column(db.Integer)
    md_tool=db.Column(db.VARCHAR)
    avg_cpu_util=db.Column(db.Float)
    max_cpu_util=db.Column(db.Float)
    avg_mem_util=db.Column(db.Float)
    max_mem_util=db.Column(db.Float)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        #return '<DATE %r>; <RELEASE %d>; <install_time %d>; <db_size %r>; <insert_time %d>; <inserted_db %r>; <report_time %r>; <report_name %r>' %(self.timestamp, self.release, self.install_time, self.db_size, self.insert_time, self.inserted_db, self.report_time, self.report_name)
        #return '<DATE %r>; <RELEASE %f>; <install_time %d>; <db_size %r>;' %(self.timestamp, self.release, self.install_time, self.db_size)
        data={}
        for key in self.__table__.columns.keys():
            val = getattr(self, key)
            if type(val) is datetime.datetime:
                val = val.strftime('%Y-%m-%dT%H:%M:%SZ')
            data[key] = val
        return json.dumps(data)

# class ML55(db.Model):
#     __table_args__ = {'sqlite_autoincrement': True}
#     __tablename__ = "ML55"
#     id=db.Column(db.Integer, primary_key=True)
#     timestamp=db.Column(db.DateTime)
#     install_time=db.Column(db.Float)
#     mldb_size=db.Column(db.VARCHAR)
#     insert_time=db.Column(db.Float)
#     inserted_db=db.Column(db.VARCHAR)
#     report_time=db.Column(db.Float)
#     report_name=db.Column(db.VARCHAR)
# 
#     def is_authenticated(self):
#         return True
# 
#     def is_active(self):
#         return True
# 
#     def is_anonymous(self):
#         return False
# 
#     def __repr__(self):
#         #return '<DATE %r>; <RELEASE %d>; <install_time %d>; <db_size %r>; <insert_time %d>; <inserted_db %r>; <report_time %r>; <report_name %r>' %(self.timestamp, self.release, self.install_time, self.db_size, self.insert_time, self.inserted_db, self.report_time, self.report_name)
#         #return '<DATE %r>; <RELEASE %f>; <install_time %d>; <db_size %r>;' %(self.timestamp, self.release, self.install_time, self.db_size)
#         data={}
#         for key in self.__table__.columns.keys():
#             val = getattr(self, key)
#             if type(val) is datetime.datetime:
#                 val = val.strftime('%Y-%m-%dT%H:%M:%SZ')
#             data[key] = val
#         return json.dumps(data)
# 
# class ML56(db.Model):
#     __table_args__ = {'sqlite_autoincrement': True}
#     __tablename__ = "ML56"
#     id=db.Column(db.Integer, primary_key=True)
#     timestamp=db.Column(db.DateTime)
#     install_time=db.Column(db.Float)
#     mldb_size=db.Column(db.VARCHAR)
#     insert_time=db.Column(db.Float)
#     inserted_db=db.Column(db.VARCHAR)
#     report_time=db.Column(db.Float)
#     report_name=db.Column(db.VARCHAR)
# 
#     def is_authenticated(self):
#         return True
# 
#     def is_active(self):
#         return True
# 
#     def is_anonymous(self):
#         return False
# 
#     def __repr__(self):
#         #return '<DATE %r>; <RELEASE %d>; <install_time %d>; <db_size %r>; <insert_time %d>; <inserted_db %r>; <report_time %r>; <report_name %r>' %(self.timestamp, self.release, self.install_time, self.db_size, self.insert_time, self.inserted_db, self.report_time, self.report_name)
#         #return '<DATE %r>; <RELEASE %f>; <install_time %d>; <db_size %r>;' %(self.timestamp, self.release, self.install_time, self.db_size)
#         data={}
#         for key in self.__table__.columns.keys():
#             val = getattr(self, key)
#             if type(val) is datetime.datetime:
#                 val = val.strftime('%Y-%m-%dT%H:%M:%SZ')
#             data[key] = val
#         return json.dumps(data)
